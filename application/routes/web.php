<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/save-feed-action', 'FeederController@saveFeedItemsAction')->name('feed.save.items.action');
Route::get('/save-feed', 'FeederController@saveFeedItems')->name('feed.save.items');
Route::get('/feed', 'FeederController@getFeedItems')->name('feed.get.items');
Route::get('/feed/{id}', 'FeederController@getFeedItem')->name('feed.get.item');
Route::get('/proxy/{img}', 'FeederController@imageProxy')->name('feed.proxy.img');
