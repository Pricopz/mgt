<!doctype html>
<html class="no-js" lang="en">

@include('general-components/head')
<body onload="endLoader()">
@include('general-components/header')

<!--error section area start-->
<div class="error_section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="mb-20">Movie Feed</h1>
                <a href="{{ route('feed.get.items') }}" title="" class="btn btn-dark-outline">
                    <i class="fa fa-arrow-left"></i> <strong>Back</strong>
                </a>

                <div class="card dark-box-shadow-sm">
                    <div class="card-header">
                        Movie Details
                    </div>
                    <div class="card-body">
                        <p class="mt-1 mb-1">
                            <strong>CLASS:</strong> {{ $feed->class }}
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>HEADLINE:</strong> {{ $feed->headline }}
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>RATING:</strong> {{ $feed->rating }} <i class="fa fa-star"></i>
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>YEAR:</strong> <i class="fa fa-calendar"></i> {{ $feed->year }}
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>GENRES:</strong>
                            @foreach ($feed->genres as $genre)
                                {{ $genre->genre }},
                            @endforeach
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>DIRECTORS:</strong>
                            @foreach ($feed->directors as $director)
                                {{ $director->name }},
                            @endforeach
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>CAST:</strong>
                            @foreach ($feed->cast as $cast)
                                {{ $cast->name }},
                            @endforeach
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>LAST UPDATE:</strong> {{ $feed->lastUpdated }}
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>DURATION:</strong> {{ App\Helpers\Helper::secToTime($feed->duration) }}
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>SYNOPSIS:</strong> {{ $feed->synopsis }}
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>BODY:</strong> {{ $feed->body }}
                        </p>
                    </div>
                </div>

                @if ($feed->viewingWindow)
                <div class="card dark-box-shadow-sm mt-20">
                    <div class="card-header">
                        VIEWING WINDOW
                    </div>
                    <div class="card-body">
                        <p class="mt-1 mb-1">
                            <strong>Start Date:</strong> {{ $feed->viewingWindow->startDate }}
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>Way to watch:</strong> {{ $feed->viewingWindow->wayToWatch }}
                        </p>
                        <p class="mt-1 mb-1">
                            <strong>End Date:</strong> {{ $feed->viewingWindow->endDate }}
                        </p>
                    </div>
                </div>
                @endif

                <div class="card dark-box-shadow-sm mt-20">
                    <div class="card-header">
                        CARDS IMAGE
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($feed->cardImages as $cardImage)
                                @if ($cardImage->path !== null)
                                    <div class="col-lg-3 text-center">
                                        <img src="{{ route('feed.proxy.img', [$cardImage->path]) }}" width="100px"/>
                                        <hr>
                                        <span style="display: block">
                                            Height: {{ $cardImage->h }} px
                                        </span>
                                        <span style="display: block">
                                            Width: {{ $cardImage->w }} px
                                        </span>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="card dark-box-shadow-sm mt-20">
                    <div class="card-header">
                        KEY ART IMAGE
                    </div>
                    <div class="card-body">
                        @foreach ($feed->keyArtImages as $keyArtImage)
                            @if ($keyArtImage->path !== null)
                                <div class="col-lg-3 text-center">
                                    <img src="{{ route('feed.proxy.img', [$keyArtImage->path]) }}" width="100px"/>
                                    <hr>
                                    <span style="display: block">
                                        Height: {{ $cardImage->h }} px
                                    </span>
                                    <span style="display: block">
                                        Width: {{ $cardImage->w }} px
                                    </span>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!--error section area end-->

@include('general-components/footer')

</body>
</html>
