<!doctype html>
<html class="no-js" lang="en">

    @include('general-components/head')
    <body onload="endLoader()">
    @include('general-components/header')

    <!--error section area start-->
    <div class="error_section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="mb-20">Movie Feeds</h1>
                    <div class="categories-page">
                        @if ($feeds)
                            @foreach ($feeds as $feed)
                                <div class="dark-box-shadow-md bg-white mb-15" style="padding: 20px">
                                    <p class="mt-1 mb-1">
                                        <strong>CLASS:</strong> {{ $feed->class }}
                                    </p>
                                    <p class="mt-1 mb-1">
                                        <strong>HEADLINE:</strong> {{ $feed->headline }}
                                    </p>
                                    <p class="mt-1 mb-1">
                                        <strong>SYNOPSIS:</strong> {{ $feed->synopsis }}
                                    </p>
                                    <p class="mt-1 mb-1">
                                        <strong>RATING:</strong> {{ $feed->rating }} <i class="fa fa-star"></i>
                                    </p>
                                    <p class="mt-1 mb-1">
                                        <strong>YEAR:</strong> <i class="fa fa-calendar"></i> {{ $feed->year }}
                                    </p>
                                    <hr>
                                    <a href="{{ route('feed.get.item', [$feed->id_inc]) }}" title="">
                                        <i class="fa fa-link"></i> <strong>Details...</strong>
                                    </a>
                                </div>
                            @endforeach

                            <div class="pagination">
                                {{ $feeds->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--error section area end-->
    @include('general-components/footer')
    </body>
</html>
