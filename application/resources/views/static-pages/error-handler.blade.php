<!doctype html>
<html class="no-js" lang="en">
    @include('general-components/head')
    <body onload="endLoader()">
    @include('general-components/header')
    <!--error section area start-->
    <div class="error_section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="error_form">
                        <h1>{{ $code }}</h1>
                        <h2><i class="fa fa-frown-o"></i> Oops! @lang('Error on page')...</h2>
                        <p>@lang('The page you are looking for does not exist, have been removed or a sad situation happened') !</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--error section area end-->
    @include('general-components/footer')
    </body>
</html>
