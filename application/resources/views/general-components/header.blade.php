<div class="full-loader-page hide">
    <div class="lds-ellipsis">
        <div></div><div></div><div></div><div></div>
        <span>@lang('Loading')...</span>
    </div>
</div>

<!-- Main Wrapper Start -->
<!--header area start-->
<header class="header_area header_padding">
    <!--header top start-->
    <div class="header_top top_two">
        <div class="container">
        </div>
    </div>
    <!--header top start-->
</header>
<!--header area end-->
