<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ \Config::get('page_title') }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google-signin-client_id" content="427980961886-rb0kmhbfa1hdkqekvjv78380s0sc5dh2.apps.googleusercontent.com">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon-x.ico') }}">

    <!-- CSS
    ========================= -->

    <!-- Plugins CSS -->
    <link href="{{ asset('css/plugins.css') }}" rel="stylesheet" type="text/css">
    <!-- Main Style CSS -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <!-- Bootstrap select CSS -->
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Typeahead CSS -->
    <link href="{{ asset('css/typeahead.css') }}" rel="stylesheet" type="text/css">

    <!-- Typeahead -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script>
        function startLoader() { $('.full-loader-page').removeClass('hide'); }
        function endLoader() { $('.full-loader-page').addClass('hide'); }
    </script>
</head>
