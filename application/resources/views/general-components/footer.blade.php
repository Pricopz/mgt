<!--footer area start-->
<footer class="footer_widgets">
    <div class="container">
        <div class="footer_top">
        </div>
        <div class="footer_bottom">
        </div>
    </div>
</footer>
<!--footer area end-->

<!-- Plugins JS -->
<script src="{{ asset('js/plugins.js') }}"></script>
<!-- Main JS -->
<script src="{{ asset('js/main.js') }}"></script>
<!-- Bootstrap Select JS -->
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
