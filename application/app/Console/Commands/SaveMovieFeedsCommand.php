<?php

namespace App\Console\Commands;

use App\Helpers\FeedJsonListener;
use App\Services\FeederService;
use App\Services\MediaService;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

/**
 * Class SaveMovieFeedsCommand
 * @package App\Console\Commands
 */
class SaveMovieFeedsCommand extends Command
{
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'feed:save';
    /**
     * @var string
     */
    private $jsonFile = 'https://mgtechtest.blob.core.windows.net/files/showcase.json';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Save movie feeds from JSON file';

    public function handle(): void
    {
        try {
            $feederService = new FeederService(new MediaService(new Client()));
            if ($file = $feederService->saveFileFromURL($this->jsonFile)) {
                $callback   = static function (array $item = null) use ($feederService): void {
                    $feederService->saveItem($item);
                };
                $listener   = new FeedJsonListener($callback);
                $stream     = fopen($file, 'r');
                try {
                    $parser = new \JsonStreamingParser\Parser($stream, $listener);
                    $parser->parse();
                    fclose($stream);
                } catch (\Exception $e) {
                    fclose($stream);
                    throw $e;
                }
            }
        } catch (\Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}
