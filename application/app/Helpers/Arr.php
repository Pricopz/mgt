<?php

namespace App\Helpers;

/**
 * Class Arr
 * @package App\Helpers
 */
class Arr
{
    /**
     * @param callable $f
     * @param array $a
     * @return array
     */
    public static function array_map_assoc(callable $f, array $a): ?array
    {
        return (array)array_reduce(
            array_map($f, array_keys($a), $a),
            static function (array $acc, array $a) {
            return $acc + $a;
        }, []);
    }

    /**
     * @param \Closure $func
     * @param array $arr
     * @return array
     */
    public static function map(\Closure $func, array $arr): array
    {
        return array_map($func, $arr);
    }

}
