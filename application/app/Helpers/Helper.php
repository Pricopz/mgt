<?php

namespace App\Helpers;

/**
 * Class Helper
 * @package App\Helpers
 */
class Helper
{
    /**
     * @param int $seconds
     * @return string
     * @throws \Exception
     */
    public static function secToTime(int $seconds): string
    {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        return $dtF->diff($dtT)->format('%h Hour, %i minutes');
    }
}
