<?php

namespace App\Helpers;
/**
 * Trait MediaHandler
 * @package App\Helpers
 */
trait MediaHandler
{
    /**
     * @var string
     */
    private $basePath = __DIR__ . '/../../__media';

    /**
     * @return null|string
     */
    public function getImageBinary(): ?string
    {
        if (!isset($this->path)) {
            return null;
        }
         return file_get_contents($this->basePath . '/__images/' . $this->path) ?: null;
    }
}
