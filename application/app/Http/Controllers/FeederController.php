<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Services\FeederService;
use App\Services\MediaService;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class FeederController
 * @package App\Http\Controllers
 */
class FeederController extends Controller
{
    /**
     * @var string
     */
    private $jsonFile = 'https://mgtechtest.blob.core.windows.net/files/showcase.json';

    /**
     * @var FeederService
     */
    private $feederService;
    /**
     * @var MediaService
     */
    private $mediaService;

    /**
     * FeederController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->mediaService     = new MediaService(new Client());
        $this->feederService    = new FeederService($this->mediaService);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFeedItems(Request $request)
    {
        $page   = $request->get('page', 1);
        $size   = $request->get('per_page', 5);
        $feeds  = $this->feederService->getCachedItems($page, $size);

        return view('feed/feed-list', ['feeds' => $feeds]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFeedItem($id)
    {
        try {
            $feed = Movie::query()->findOrFail($id);
            return view('feed/feed-item', ['feed' => $feed]);
        } catch (ModelNotFoundException $e) {
            throw new HttpException(404);
        }
    }

    /**
     * @param $img
     * @return Response
     */
    public function imageProxy($img): Response
    {
        return $this->mediaService->imageProxy($img, 3600);
    }
}
