<?php

namespace App\Enum;

/**
 * Class GenresEnum
 * @package App\Enum
 */
abstract class GenresEnum extends Enum
{
    public const COMEDY         = 'Comedy';
    public const FAMILY         = 'Family';
    public const CLASSICS       = 'Classics';
    public const MYSTERY        = 'Mystery';
    public const ROMANCE        = 'Romance';
    public const THRILLER       = 'Thriller';
    public const ACTION_ADVENTURE = 'Action & Adventure';
    public const SF_FANTASY     = 'Sci-fi/Fantasy';
    public const DRAMA          = 'Drama';
    public const WORLD_CINEMA   = 'World Cinema';
    public const HORROR         = 'Horror';
    public const ANIMATION      = 'Animation';
}
