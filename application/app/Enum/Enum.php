<?php


namespace App\Enum;


abstract class Enum
{
    /**
     * @return array
     */
    public static function asMap(): array
    {
        try {
            $rc = new \ReflectionClass(static::class);
            return $rc->getConstants();
        } catch (\ReflectionException $e) {
            return [];
        }
    }

    /**
     * @return array
     */
    public static function asListKeys(): array
    {
        return array_keys(static::asMap());
    }

    /**
     * @return array
     */
    public static function asListValues(): array
    {
        return array_values(static::asMap());
    }
}
