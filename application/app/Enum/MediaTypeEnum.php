<?php

namespace App\Enum;

/**
 * Class MediaTypeEnum
 * @package App\Enum
 */
abstract class MediaTypeEnum extends Enum
{
    public const IMG_CARD   = 'img_cart';
    public const IMG_KEY_ART= 'img_key_art';
    public const VID_VID    = 'vid_vid';
    public const VID_VID_ALT= 'vid_vid_alt';
}
