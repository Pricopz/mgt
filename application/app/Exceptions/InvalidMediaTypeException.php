<?php

namespace App\Exceptions;

use Throwable;

/**
 * Class InvalidMediaTypeException
 * @package App\Exceptions
 */
class InvalidMediaTypeException extends \Exception
{
    /**
     * InvalidMediaTypeException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Invalid media type', $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
