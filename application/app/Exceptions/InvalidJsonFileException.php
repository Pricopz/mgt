<?php

namespace App\Exceptions;

use Throwable;

/**
 * Class InvalidJsonFileException
 * @package App\Exceptions
 */
class InvalidJsonFileException extends \Exception
{
    /**
     * InvalidJsonFileException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Invalid JSON file', $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
