<?php

namespace App\Exceptions;

use Throwable;

/**
 * Class ModelNotSavedException
 * @package App\Exceptions
 */
class ModelNotSavedException extends \Exception
{
    /**
     * ModelNotSavedException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Model not saved', $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
