<?php

namespace App\Services;

use App\Enum\MediaTypeEnum;
use App\Exceptions\InvalidJsonFileException;
use App\Exceptions\ModelNotSavedException;
use App\Exceptions\InvalidMediaTypeException;
use App\Models\CardImage;
use App\Models\Cast;
use App\Models\Director;
use App\Models\Genre;
use App\Models\KeyArtImage;
use App\Models\Movie;
use App\Models\Video;
use App\Models\VideoAlternative;
use App\Models\ViewingWindow;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class FeederService
 * @package App\Services
 */
class FeederService
{
    /**
     * @var string
     */
    private $basePath;
    /**
     * @var MediaService
     */
    private $mediaService;

    /**
     * FeederService constructor.
     * @param MediaService $mediaService
     */
    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
        $this->basePath     = __DIR__ . '/../../' . env('UPLOADS_FILES_DIR', '__files');
    }

    /**
     * @param string $basePath
     * @throws \Exception
     */
    public function setBasePath(string $basePath): void
    {
        if (is_dir($basePath)) {
            $this->basePath = $basePath;
            return;
        }
        throw new \Exception("Invalid DIR: {$basePath}");
    }

    /**
     * @param string $url
     * @return string
     */
    public function getFileName(string $url): string
    {
        $path   = parse_url($url, PHP_URL_PATH);
        $elem   = explode('/', $path);
        $file   = end($elem);
        $fileElem = explode('.', $file);
        $ext    = array_pop($fileElem);
        return 'file_' . sha1(implode('_', $fileElem) . '_' . uniqid(mt_rand(), true)) . '.' . $ext;
    }


    /**
     * @param string $url
     * @return string
     * @throws InvalidJsonFileException
     * @throws \ErrorException
     */
    public function saveFileFromURL(string $url): string
    {
        if ($content = file_get_contents($url)) {
            $filePath = $this->basePath . '/' . $this->getFileName($url);
            if (file_put_contents($filePath, $content)) {
                return $filePath;
            }
            throw new \ErrorException('Unable to save file to disk', 500);
        }
        throw new InvalidJsonFileException();
    }

    /**
     * @param int $page
     * @param int $size
     * @return LengthAwarePaginator
     */
    public function getItems(int $page = 1, int $size = 2): LengthAwarePaginator
    {
        return Movie::query()->paginate($size, ['*'], 'page', $page);
    }

    /**
     * @param int $page
     * @param int $size
     * @param int $expiresAfter
     * @return mixed
     */
    public function getCachedItems(int $page = 1, int $size = 2, int $expiresAfter = 3600)
    {
        $expiresAt  = now()->addSeconds($expiresAfter);
        $key        = "key_movie_feeds_page_{$page}_size_{$size}";
        $class      = $this;
        return Cache::remember(
            $key,
            $expiresAt,
            static function() use ($class, $page, $size) {
                return $class->getItems($page, $size);
            }
        );
    }

    /**
     * @param array|null $data
     */
    public function saveItem(array $data = null): void
    {
        if (empty($data)) {
            echo 'Empty data, exit!' . PHP_EOL;
            return;
        }
        try {
            DB::beginTransaction();

            // Save Movie
            $movie          = Movie::fromArray($data);
            $movie->body    = $this->formatText($movie->body);
            $movie->quote   = $this->formatText($movie->quote);
            $movie->synopsis= $this->formatText($movie->synopsis);
            if (!$movie->save()) {
                throw new ModelNotSavedException('Movie not saved', 500);
            }

            // Save Card Images
            $this->saveCardImages($movie, $data['cardImages'] ?? []);

            // Save Card Images
            $this->saveKeyArtImages($movie, $data['keyArtImages'] ?? []);

            // Save Cast
            $this->saveCast($movie, $data['cast'] ?? []);

            // Save Directors
            $this->saveDirectors($movie, $data['directors'] ?? []);

            // Save Genres
            $this->saveGenres($movie, $data['genres'] ?? []);

            // Save Videos
            $this->saveVideos($movie, $data['videos'] ?? []);

            // Save Viewing Window
            if (!$this->saveViewingWindow($movie, $data['viewingWindow'] ?? null)) {
                throw new ModelNotSavedException('Viewing Windows not saved', 500);
            }

            // Commit transaction
            DB::commit();

            Log::info("Movie with ID $movie->id was saved!");
            echo "Movie with ID $movie->id was saved!" . PHP_EOL;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Error on saving movie: ' . $e->getMessage());
            echo 'Error on saving Movie. Check log for more information!' . PHP_EOL;
        }
    }

    /**
     * @param string|null $text
     * @return string|null
     */
    public function formatText(string $text = null): ?string
    {
        if ($text !== null) {
            return mb_convert_encoding($text, 'UTF-8', 'Windows-1252') ?: $text;
        }
        return null;
    }

    /**
     * @param Movie $movie
     * @param array $cardImages
     * @throws ModelNotSavedException
     * @throws InvalidMediaTypeException
     */
    private function saveCardImages(Movie $movie, array $cardImages): void
    {
        foreach ($cardImages as $cardImage) {
            $cardImg            = CardImage::fromArray($cardImage);
            $cardImg->movie_id  = $movie->id_inc;
            $cardImg->path      = $this->mediaService->saveImage($cardImg, MediaTypeEnum::IMG_CARD);

            if (!$cardImg->save()) {
                throw new ModelNotSavedException('Card Image not saved', 500);
            }
        }
    }

    /**
     * @param Movie $movie
     * @param array $keyArtImages
     * @throws ModelNotSavedException
     * @throws InvalidMediaTypeException
     */
    private function saveKeyArtImages(Movie $movie, array $keyArtImages): void
    {
        foreach ($keyArtImages as $keyArtImage) {
            $keyArtImg            = KeyArtImage::fromArray($keyArtImage);
            $keyArtImg->movie_id  = $movie->id_inc;
            $keyArtImg->path      = $this->mediaService->saveImage($keyArtImg, MediaTypeEnum::IMG_KEY_ART);

            if (!$keyArtImg->save()) {
                throw new ModelNotSavedException('Key Art Image not saved', 500);
            }
        }
    }

    /**
     * @param Movie $movie
     * @param array $casts
     * @throws ModelNotSavedException
     */
    private function saveCast(Movie $movie, array $casts): void
    {
        foreach ($casts as $cast) {
            $name = isset($cast['name']) ? $this->formatText($cast['name']) : null;
            if ($name && $castModel = Cast::findOrSaveByName($name)) {
                // Attach
                $movie->cast()->attach($castModel->id_inc);
            } else {
                throw new ModelNotSavedException('Cast not saved', 500);
            }
        }
    }

    /**
     * @param Movie $movie
     * @param array $directors
     * @throws ModelNotSavedException
     */
    private function saveDirectors(Movie $movie, array $directors): void
    {
        foreach ($directors as $director) {
            $name = isset($director['name']) ? $this->formatText($director['name']) : null;
            if ($name && $castModel = Director::findOrSaveByName($name)) {
                // Attach
                $movie->directors()->attach($castModel->id_inc);
            } else {
                throw new ModelNotSavedException('Director not saved', 500);
            }
        }
    }

    /**
     * @param Movie $movie
     * @param array $genres
     * @throws ModelNotSavedException
     */
    private function saveGenres(Movie $movie, array $genres): void
    {
        foreach ($genres as $genre) {
            if ($genreModel = Genre::findOrSaveGenre($genre)) {
                // Attach
                $movie->genres()->attach($genreModel->id_inc);
            } else {
                throw new ModelNotSavedException('Genre not saved', 500);
            }
        }
    }

    /**
     * @param Movie $movie
     * @param array $videos
     * @throws InvalidMediaTypeException
     * @throws ModelNotSavedException
     */
    private function saveVideos(Movie $movie, array $videos): void
    {
        foreach ($videos as $video) {
            $videoModel           = Video::fromArray($video);
            $videoModel->movie_id = $movie->id_inc;
            if (!$videoModel->save()) {
                throw new ModelNotSavedException('Video not saved', 500);
            }
            $this->saveVideoAlternatives($videoModel, $video['alternatives'] ?? []);
        }
    }

    /**
     * @param Video $video
     * @param array $alternatives
     * @throws ModelNotSavedException
     * @throws InvalidMediaTypeException
     */
    private function saveVideoAlternatives(Video $video, array $alternatives): void
    {
        foreach ($alternatives as $alternative) {
            $videoAlternative           = VideoAlternative::fromArray($alternative);
            $videoAlternative->video_id = $video->id_inc;
            $videoAlternative->path     = $this->mediaService->saveVideo($videoAlternative, MediaTypeEnum::VID_VID_ALT);

            if (!$videoAlternative->save()) {
                throw new ModelNotSavedException('Video Alternative not saved', 500);
            }
        }
    }

    /**
     * @param Movie $movie
     * @param array|null $data
     * @return bool
     */
    private function saveViewingWindow(Movie $movie, array $data = null): bool
    {
        if ($data === null) {
            return true; // Don`t have view window...
        }
        $viewMode = ViewingWindow::fromArray($data);
        $viewMode->movie_id = $movie->id_inc;

        return $viewMode->save();
    }
}
