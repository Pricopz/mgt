<?php

namespace App\Services;

use App\Enum\MediaTypeEnum;
use App\Exceptions\InvalidMediaTypeException;
use App\Models\Image;
use App\Models\MediaModel;
use App\Models\VideoAlternative;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

/**
 * Class MediaService
 * @package App\Services
 */
class MediaService
{
    /**
     * @var string
     */
    private $basePath;

    /**
     * @var
     */
    private $httpClient;

    /**
     * MediaService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->httpClient   = $client;
        $this->basePath     = __DIR__ . '/../../' . env('UPLOADS_MEDIA_DIR', '__media');
    }

    /**
     * @param string $basePath
     * @throws \Exception
     */
    public function setBasePath(string $basePath): void
    {
        if (is_dir($basePath)) {
            $this->basePath = $basePath;
            return;
        }
        throw new \Exception("Invalid DIR: {$basePath}");
    }

    /**
     * @param string $url
     * @param string $filePathOnServer
     * @return bool
     */
    public function downloadFileFromURL(string $url, string $filePathOnServer): bool
    {
        try {
            $response = $this->httpClient->get($url);
            if ($response->getStatusCode() === 200) {
                if (is_dir($filePathOnServer)) {
                    return file_put_contents($filePathOnServer, $response->getBody()->getContents());
                } else {
                    throw new \Exception("Invalid DIR: {$filePathOnServer}");
                }
            }
            return false;
        } catch (\Exception $e) {
            Log::error($e->getMessage(), (array)$e);
            return false;
        }
    }

    /**
     * @param Image $image
     * @param string $type
     * @return null|string
     * @throws InvalidMediaTypeException
     */
    public function saveImage(Image $image, string $type): ?string
    {
        if (!$this->isValidMediaType($type)) {
            throw new InvalidMediaTypeException('Invalid media type: ' . $type);
        }
        $fileName = $this->getImageName($image, $type);

        if ($this->downloadFileFromURL($image->url, $this->basePath . '/__images/' . $fileName)) {
            return $fileName;
        }
        return null;
    }

    /**
     * @param VideoAlternative $videoAlternative
     * @param string $type
     * @return string|null
     * @throws InvalidMediaTypeException
     */
    public function saveVideo(VideoAlternative $videoAlternative, string $type): ?string
    {
        if (!$this->isValidMediaType($type)) {
            throw new InvalidMediaTypeException('Invalid media type: ' . $type);
        }

        $fileName = $this->getVideoName($videoAlternative, $type);

        if ($this->downloadFileFromURL($videoAlternative->url, $this->basePath . '/__videos/' . $fileName)) {
            return $fileName;
        }
        return null;
    }

    /**
     * @param Image $image
     * @param string $type
     * @return string
     */
    private function getImageName(Image $image, string $type): string
    {
        return 'img_' . sha1("{$type}_w_{$image->w}_h_{$image->h}_unq_" . uniqid(mt_rand(), true)) . '.' . pathinfo($image->url, PATHINFO_EXTENSION);
    }

    /**
     * @param VideoAlternative $videoAlternative
     * @param string $type
     * @return string
     */
    private function getVideoName(VideoAlternative $videoAlternative, string $type): string
    {
        return 'vid_' . sha1("{$type}_q_{$videoAlternative->quality}_unq_" . uniqid(mt_rand(), true)) . '.' . pathinfo($videoAlternative->url, PATHINFO_EXTENSION) ?? 'mp4';
    }

    /**
     * @param string $type
     * @return bool
     */
    private function isValidMediaType(string $type): bool
    {
        return in_array($type, MediaTypeEnum::asListValues(), true);
    }

    /**
     * @param string $image
     * @return string
     */
    private function getImageFullPath(string $image): string
    {
        return $this->basePath . '/__images/' . $image;
    }

    /**
     * @param string $image
     * @return MediaModel|null
     */
    private function getImageMediaModel(string $image): ?MediaModel
    {
        $file = $this->getImageFullPath($image);
        if (file_exists($file)) {
            return new MediaModel(
                file_get_contents($file),
                filesize($file),
                mime_content_type($file)
            );
        }
        return null;
    }

    /**
     * @param string $image
     * @param int $expiresAfter
     * @return MediaModel|null
     */
    private function getImageMediaModelCached(string $image, int $expiresAfter = 60): ?MediaModel
    {
        $file = $this->getImageFullPath($image);
        if (file_exists($file)) {
            $expiresAt  = now()->addSeconds($expiresAfter);
            $key        = 'key_img_' . md5($image);
            $class      = $this;
            return Cache::remember(
                $key,
                $expiresAt,
                static function() use ($class, $image) {
                    return $class->getImageMediaModel($image);
                }
            );
        }
        return null;
    }

    /**
     * @param MediaModel|null $mediaModel
     * @return \Illuminate\Http\Response
     */
    public function getMediaModelResponse(MediaModel $mediaModel = null): \Illuminate\Http\Response
    {
        if ($mediaModel) {
            $response = Response::make($mediaModel->getContent(), 200);
            $response->header('Content-Type', $mediaModel->getContentType());
            $response->header('Content-Length', $mediaModel->getFileSize());
        } else {
            $response = Response::make('', 404);
            $response->header('Content-Type', 'image/jpg');
        }
        return $response;
    }

    /**
     * @param string $image
     * @param int $cacheTime
     * @return \Illuminate\Http\Response
     */
    public function imageProxy(string $image, int $cacheTime = 0): \Illuminate\Http\Response
    {
        if ($cacheTime > 0) {
            return $this->getMediaModelResponse($this->getImageMediaModelCached($image, $cacheTime));
        }
        return $this->getMediaModelResponse($this->getImageMediaModel($image));
    }
}
