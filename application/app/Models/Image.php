<?php

namespace App\Models;

use App\Helpers\MediaHandler;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App\Models
 *
 * @property integer $id_inc
 * @property integer $movie_id
 * @property string $url
 * @property integer $h
 * @property integer $w
 * @property string $path
 */
abstract class Image extends Model
{
    use MediaHandler;

    protected $primaryKey = 'id_inc';

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        $model          = new static();
        $model->movie_id= $data['movie_id'] ?? null;
        $model->url     = $data['url'] ?? null;
        $model->h       = $data['h'] ?? null;
        $model->w       = $data['w'] ?? null;
        $model->path    = $data['path'] ?? null;

        return $model;
    }
}
