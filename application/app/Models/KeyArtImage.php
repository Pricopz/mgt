<?php

namespace App\Models;

/**
 * Class KeyArtImage
 * @package App\Models
 */
class KeyArtImage extends Image
{
    protected $table = 'key_art_images';
}
