<?php

namespace App\Models;

/**
 * Class Cast
 * @package App\Models
 *
 * @property int $id_inc
 * @property string $name
 */
class Director extends Human
{
    protected $table = 'directors';

    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'movie_director', 'director_id', 'movie_id');
    }
}
