<?php

namespace App\Models;

/**
 * Class Cast
 * @package App\Models
 *
 * @property int $id_inc
 * @property string $name
 */
class Cast extends Human
{
    protected $table = 'cast';

    public function movies()
    {
        return $this->belongsToMany(Movie::class,'movie_cast', 'cast_id', 'movie_id');
    }
}
