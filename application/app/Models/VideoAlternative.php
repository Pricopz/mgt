<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class VideoAlternative
 * @package App\Models
 *
 * @property integer $id_inc
 * @property integer $video_id
 * @property string $quality
 * @property string $url
 * @property string $path
 */
class VideoAlternative extends Model
{
    protected $table = 'video_alternatives';

    protected $primaryKey = 'id_inc';

    /**
     * @param array $data
     * @return VideoAlternative
     */
    public static function fromArray(array $data): VideoAlternative
    {
        $videoAlternative           = new self();
        $videoAlternative->video_id = $data['video_id'] ?? null;
        $videoAlternative->quality  = $data['quality'] ?? null;
        $videoAlternative->url      = $data['url'] ?? null;
        $videoAlternative->path     = $data['path'] ?? null;

        return $videoAlternative;
    }
}
