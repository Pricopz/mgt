<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ViewingWindow
 * @package App\Models
 *
 * @property integer $id_inc
 * @property integer $movie_id
 * @property string $startDate
 * @property string $wayToWatch
 * @property string $endDate
 */
class ViewingWindow extends Model
{
    protected $table = 'viewing_window';

    protected $primaryKey = 'id_inc';

    /**
     * @param array $data
     * @return ViewingWindow
     */
    public static function fromArray(array $data): ViewingWindow
    {
        $viewingWindow              = new self();
        $viewingWindow->movie_id    = $data['movie_id'] ?? null;
        $viewingWindow->startDate   = $data['startDate'] ?? null;
        $viewingWindow->wayToWatch  = $data['wayToWatch'] ?? null;
        $viewingWindow->endDate     = $data['endDate'] ?? null;

        return $viewingWindow;
    }
}
