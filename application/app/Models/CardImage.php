<?php

namespace App\Models;

/**
 * Class CardImage
 * @package App\Models
 */
class CardImage extends Image
{
    protected $table = 'card_images';
}
