<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
/**
 * Class Movie
 * @package App\Models
 *
 * @property int $id_inc
 * @property string $body
 * @property string $cert
 * @property string $class
 * @property int $duration
 * @property string $headline
 * @property string $id
 * @property string $lastUpdated
 * @property string $quote
 * @property string $rating
 * @property string $reviewAuthor
 * @property string $skyGoId
 * @property string $skyGoUrl
 * @property string $sum
 * @property string $synopsis
 * @property string $url
 * @property string $year
 *
 * @property CardImage[] $cardImages
 * @property Cast[] $cast
 * @property Director[] $directors
 * @property Genre[] $genres
 * @property KeyArtImage[] $keyArtImages
 * @property Video[] $videos
 * @property ViewingWindow $viewingWindow
 */
class Movie extends Model
{
    protected $table = 'movies';

    protected $primaryKey = 'id_inc';

    /**
     * @return HasMany
     */
    public function cardImages(): HasMany
    {
        return $this->hasMany(CardImage::class, 'movie_id', 'id_inc');
    }

    /**
     * @return HasMany
     */
    public function keyArtImages(): HasMany
    {
        return $this->hasMany(KeyArtImage::class, 'movie_id', 'id_inc');
    }

    /**
     * @return HasMany
     */
    public function videos(): HasMany
    {
        return $this->hasMany(Video::class, 'movie_id', 'id_inc');
    }

    /**
     * @return HasOne
     */
    public function viewingWindow(): HasOne
    {
        return $this->hasOne(ViewingWindow::class, 'movie_id', 'id_inc');
    }

    /**
     * @return BelongsToMany
     */
    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class, 'movie_genre', 'movie_id', 'genre_id');
    }

    /**
     * @return BelongsToMany
     */
    public function directors(): BelongsToMany
    {
        return $this->belongsToMany(Director::class, 'movie_director', 'movie_id', 'director_id');
    }

    /**
     * @return BelongsToMany
     */
    public function cast(): BelongsToMany
    {
        return $this->belongsToMany(Cast::class, 'movie_cast', 'movie_id', 'cast_id');
    }

    /**
     * @param array $data
     * @return Movie
     */
    public static function fromArray(array $data): Movie
    {
        $movie              = new self();
        $movie->body        = $data['body'] ?? null;
        $movie->cert        = $data['cert'] ?? null;
        $movie->class       = $data['class'] ?? null;
        $movie->duration    = $data['duration'] ?? null;
        $movie->headline    = $data['headline'] ?? null;
        $movie->id          = $data['id'] ?? null;
        $movie->lastUpdated = $data['lastUpdated'] ?? null;
        $movie->quote       = $data['quote'] ?? null;
        $movie->rating      = $data['rating'] ?? null;
        $movie->reviewAuthor= $data['reviewAuthor'] ?? null;
        $movie->skyGoId     = $data['skyGoId'] ?? null;
        $movie->skyGoUrl    = $data['skyGoUrl'] ?? null;
        $movie->sum         = $data['sum'] ?? null;
        $movie->synopsis    = $data['synopsis'] ?? null;
        $movie->url         = $data['url'] ?? null;
        $movie->year        = $data['year'] ?? null;

        return $movie;
    }
}
