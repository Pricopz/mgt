<?php

namespace App\Models;

/**
 * Class MediaModel
 * @package App\Models
 */
class MediaModel
{
    /**
     * @var
     */
    private $content;
    /**
     * @var
     */
    private $contentType;
    /**
     * @var integer
     */
    private $fileSize;

    /**
     * @return int
     */
    public function getFileSize(): int
    {
        return $this->fileSize;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * MediaModel constructor.
     * @param $content
     * @param int $fileSize
     * @param string $contentType
     */
    public function __construct($content, int $fileSize, string $contentType = 'image/jpg')
    {
        $this->content      = $content;
        $this->fileSize     = $fileSize;
        $this->contentType  = $contentType;
    }
}
