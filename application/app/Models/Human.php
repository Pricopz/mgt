<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cast
 * @package App\Models
 *
 * @property int $id_inc
 * @property string $name
 */
class Human extends Model
{
    protected $primaryKey = 'id_inc';

    /**
     * @param string $name
     * @return Human|\Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    public static function findOrSaveByName(string $name)
    {
        if ($model = static::query()->where('name', '=', $name)->first()) {
            return $model;
        }
        $model = new static();
        $model->name = $name;
        if ($model->save()) {
            return $model;
        }
        return null;
    }
}
