<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Genre
 * @package App\Models
 *
 * @property integer $id_inc
 * @property string $genre
 */
class Genre extends Model
{
    protected $table = 'genres';

    protected $primaryKey = 'id_inc';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'movie_genre', 'genre_id', 'movie_id');
    }

    /**
     * @param string $genre
     * @return Human|\Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    public static function findOrSaveGenre(string $genre)
    {
        if ($model = static::query()->where('genre', '=', $genre)->first()) {
            return $model;
        }
        $model          = new static();
        $model->genre   = $genre;
        if ($model->save()) {
            return $model;
        }
        return null;
    }
}
