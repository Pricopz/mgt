<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Video
 * @package App\Models
 *
 * @property integer $id_inc
 * @property integer $movie_id
 * @property string $title
 * @property string $type
 * @property string $url
 *
 * @property VideoAlternative[] $alternatives
 */
class Video extends Model
{
    protected $table = 'videos';

    protected $primaryKey = 'id_inc';

    /**
     * @return HasMany
     */
    public function alternatives(): HasMany
    {
        return $this->hasMany(VideoAlternative::class, 'video_id', 'id_inc');
    }

    /**
     * @param array $data
     * @return Video
     */
    public static function fromArray(array $data): Video
    {
        $video          = new self();
        $video->movie_id= $data['movie_id'] ?? null;
        $video->title   = $data['title'] ?? null;
        $video->type    = $data['type'] ?? null;
        $video->url     = $data['url'] ?? null;

        return $video;
    }
}
