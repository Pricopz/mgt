<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoAlternativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_alternatives', static function (Blueprint $table) {
            $table->bigIncrements('id_inc');
            $table->integer('video_id')->index();
            $table->string('quality');
            $table->string('url', 2048);
            $table->string('path', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_alternatives');
    }
}
