<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', static function (Blueprint $table) {
            $table->bigIncrements('id_inc');
            $table->text('body')->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
            $table->string('cert');
            $table->string('class');
            $table->integer('duration');
            $table->string('headline')->nullable();
            $table->string('id')->nullable();
            $table->date('lastUpdated');
            $table->string('quote')->charset('utf8mb4')->collation('utf8mb4_unicode_ci')->nullable();
            $table->smallInteger('rating')->unsigned()->nullable()->default(0);
            $table->string('reviewAuthor')->nullable();
            $table->string('skyGoId')->nullable();
            $table->string('skyGoUrl')->nullable();
            $table->string('sum')->nullable();
            $table->text('synopsis')->charset('utf8mb4')->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('url');
            $table->year('year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
