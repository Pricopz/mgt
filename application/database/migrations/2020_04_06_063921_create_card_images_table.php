<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_images', static function (Blueprint $table) {
            $table->bigIncrements('id_inc');
            $table->integer('movie_id')->unsigned()->index();
            $table->string('url',2048);
            $table->smallInteger('h')->unsigned();
            $table->smallInteger('w')->unsigned();
            $table->string('path', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_images');
    }
}
