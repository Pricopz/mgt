<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeyArtImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('key_art_images', static function (Blueprint $table) {
            $table->bigIncrements('id_inc');
            $table->integer('movie_id')->unsigned()->index();
            $table->string('url',2048);
            $table->smallInteger('h')->unsigned();
            $table->smallInteger('w')->unsigned();
            $table->string('path', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('key_art_images');
    }
}
