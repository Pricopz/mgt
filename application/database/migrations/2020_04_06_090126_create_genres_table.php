<?php

use App\Enum\GenresEnum;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genres', static function (Blueprint $table) {
            $table->bigIncrements('id_inc');
            $table->string('genre');
            $table->timestamps();
        });

        foreach (GenresEnum::asListValues() as $genre) {
            DB::table('genres')->insert(
                [
                    'genre' => $genre
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genres');
    }
}
