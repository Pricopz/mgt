<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Movie;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Movie::class, static function (Faker $faker) {
    return [
        'body' => $faker->text,
        'cert' => Str::random(10),
        'class' => Str::random(10),
        'duration' => random_int(100, 200),
        'headline' => Str::random(50),
        'id' => Str::random(20),
        'lastUpdated' => $faker->date(),
        'quote' => Str::random(50),
        'rating' => 5,
        'reviewAuthor' => Str::random(50),
        'sum' => Str::random(10),
        'synopsis' => Str::random(10),
        'url' => $faker->url,
        'year' => $faker->year,
    ];
});
