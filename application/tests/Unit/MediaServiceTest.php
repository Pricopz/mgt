<?php

namespace Tests\Unit;

use App\Enum\MediaTypeEnum;
use App\Models\MediaModel;
use App\Services\FeederService;
use App\Services\MediaService;
use GuzzleHttp\Client;
use Tests\Helpers\PHPUnitHelper;
use Tests\TestCase;

class MediaServiceTest extends TestCase
{

    private function getHttpClientMock()
    {
        /**
         * @var Client $mediaServiceMock
         */
        return $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();
    }


    public function testSetBasePathWrong()
    {
        $service= new MediaService($this->getHttpClientMock());
        $path = __DIR__ . '/../../__files_testing/wrong_dir';
        $this->expectException(\Exception::class);
        $service->setBasePath($path);
    }

    public function testSetBasePathCorrectly()
    {
        $service= new MediaService($this->getHttpClientMock());
        $path = __DIR__ . '/../../__media_testing/__test';
        $service->setBasePath($path);
        $this->assertTrue(true);
    }

    public function testIsNotValidMediaType()
    {
        $service = new MediaService($this->getHttpClientMock());
        $result = PHPUnitHelper::callMethod($service, 'isValidMediaType', ['invalid']);
        $this->assertFalse($result);
    }

    public function testIsValidMediaType()
    {
        $service = new MediaService($this->getHttpClientMock());
        $result = PHPUnitHelper::callMethod($service, 'isValidMediaType', [MediaTypeEnum::IMG_KEY_ART]);
        $this->assertTrue($result);
    }

    public function testGetImageMediaModelNotNull()
    {
        $path = __DIR__ . '/../../__media_testing/';
        $service = new MediaService($this->getHttpClientMock());
        $service->setBasePath($path);
        $result = PHPUnitHelper::callMethod($service, 'getImageMediaModel', ['../__test/sample.jpg']);
        $this->assertInstanceOf(MediaModel::class, $result);
        $this->assertEquals('image/jpeg', $result->getContentType());
        $this->assertEquals(101815, $result->getFileSize());
    }

    public function testGetImageMediaModelNull()
    {
        $path = __DIR__ . '/../../__media_testing/';
        $service = new MediaService($this->getHttpClientMock());
        $service->setBasePath($path);
        $result = PHPUnitHelper::callMethod($service, 'getImageMediaModel', ['sample.jpg']);
        $this->assertNull($result);
    }

    public function testGetMediaModelResponse200()
    {
        $service = new MediaService($this->getHttpClientMock());
        $response = $service->getMediaModelResponse(new MediaModel('content', 10));
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetMediaModelResponse404()
    {
        $service = new MediaService($this->getHttpClientMock());
        $response = $service->getMediaModelResponse(null);
        $this->assertEquals(404, $response->getStatusCode());
    }
}
