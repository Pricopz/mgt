<?php

namespace Tests\Unit;

use App\Models\Director;
use App\Models\KeyArtImage;
use App\Models\Movie;
use App\Models\ViewingWindow;
use App\Services\FeederService;
use App\Services\MediaService;
use Tests\Helpers\PHPUnitHelper;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FeederServiceTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->insertData();
    }

    /**
     * Insert data test
     */
    private function insertData(): void
    {
        $movies = factory(Movie::class, 5)
            ->create()
            ->each(static function (Movie $movie) {
                $movie->directors()->save(factory(Director::class)->make());
                $movie->viewingWindow()->save(factory(ViewingWindow::class)->make());
            });
    }

    private function getMediaServiceMock()
    {
        /**
         * @var MediaService $mediaServiceMock
         */
        return $this->getMockBuilder(MediaService::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testSetBasePathWrong()
    {
        $service= new FeederService($this->getMediaServiceMock());
        $path = __DIR__ . '/../../__files_testing/wrong_dir';
        $this->expectException(\Exception::class);
        $service->setBasePath($path);
    }

    public function testSetBasePathCorrectly()
    {
        $service= new FeederService($this->getMediaServiceMock());
        $path = __DIR__ . '/../../__media_testing/__test';
        $service->setBasePath($path);
        $this->assertTrue(true);
    }

    public function testGetFileName()
    {
        $service= new FeederService($this->getMediaServiceMock());
        $result = $service->getFileName('https://example.ro/donwloads/sample.json');
        $this->assertContains('.json', $result);
    }

    public function testSaveFileFromUrlThrowsException()
    {
        $service= new FeederService($this->getMediaServiceMock());
        $this->expectException(\ErrorException::class);
        $service->saveFileFromURL('https://example.ro/donwloads/sample.json');
    }

    public function testSaveFileFromUrlSuccess()
    {
        $file = 'https://mgtechtest.blob.core.windows.net/files/showcase.json';
        $service= new FeederService($this->getMediaServiceMock());
        $f = $service->saveFileFromURL($file);
        $this->assertFileExists($f);
        @unlink($f);
    }

    public function testFormatTextOnNull()
    {
        $service= new FeederService($this->getMediaServiceMock());
        $result = $service->formatText();
        $this->assertNull($result);
    }

    public function testFormatTextOnNotNull()
    {
        $text = 'Tokyo';
        $service= new FeederService($this->getMediaServiceMock());
        $result = $service->formatText($text);
        $this->assertEquals('Tokyo', $result);
    }

    public function testGetItems()
    {
        $service = new FeederService($this->getMediaServiceMock());
        $paginator = $service->getItems(1, 3);

        $this->assertInstanceOf(LengthAwarePaginator::class, $paginator);
        $this->assertEquals(5, $paginator->total());
        $this->assertCount(3, $paginator->items());
    }

    public function testSaveKeyArtImages()
    {
        $movie = Movie::query()->first();
        $service = new FeederService($this->getMediaServiceMock());
        $keyArtImagesArr = [
            [
                'url' => 'http://sample.ro/img.jpg',
                'h' => 100,
                'w' => 200,
            ]
        ];
        PHPUnitHelper::callMethod($service, 'saveKeyArtImages', [$movie, $keyArtImagesArr]);
        $images = KeyArtImage::query()->where('movie_id', '=', $movie->id_inc)->get()->all();
        $this->assertCount(1, $images);
        $this->assertEquals('http://sample.ro/img.jpg', $images[0]->url);
    }
}
