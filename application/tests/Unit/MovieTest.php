<?php

namespace Tests\Unit;

use App\Models\Director;
use App\Models\Movie;
use App\Models\ViewingWindow;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MovieTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->insertData();
    }

    /**
     * Insert data test
     */
    private function insertData(): void
    {
        factory(Movie::class, 5)
            ->create()
            ->each(static function (Movie $movie) {
                $movie->directors()->save(factory(Director::class)->make());
                $movie->directors()->save(factory(Director::class)->make());
                $movie->viewingWindow()->save(factory(ViewingWindow::class)->make());
            });
    }


    public function testRelations()
    {
        $m = Movie::all();
        $this->assertCount(5, $m);
        $this->assertCount(2, $m[0]->directors);
        $this->assertNotNull($m[0]->viewingWindow);
    }

    public function testFromArray()
    {
        $arr = [
            'body' => 'test body',
            'year' => 2000,
        ];
        $movie = Movie::fromArray($arr);
        $this->assertInstanceOf(Movie::class, $movie);
        $this->assertEquals('test body', $movie->body);
        $this->assertEquals(2000, $movie->year);
    }

    public function testSaveFromArray()
    {
        $arr = [
            'body' => 'body',
            'cert' => 'cert',
            'class' => 'movie',
            'duration' => 10,
            'headline' => 'headline',
            'id' => 'id',
            'lastUpdated' => '2020-01-01',
            'quote' => 'url',
            'rating' => 5,
            'reviewAuthor' => 'ass',
            'sum' => 'url',
            'synopsis' => 'url',
            'url' => 'url',
            'year' => 2000,
        ];
        $movie = Movie::fromArray($arr);
        $this->assertTrue($movie->save());
        $this->assertNotNull($movie->id_inc);
        $this->assertTrue($movie->delete());
    }

    public function testMovies()
    {
        $movies = factory(Movie::class, 2)->make();
        foreach ($movies as $movie) {
            $movie->save();
        }
        $this->assertCount(7, Movie::all());
    }
}
