<?php

namespace Tests\Unit;

use App\Models\CardImage;
use App\Models\KeyArtImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ImageTest extends TestCase
{
    use RefreshDatabase;

    public function testCartImageFromArray()
    {
        $arr = [
            'movie_id' => 1,
            'url' => 'https://example.com',
            'h' => 680,
            'w' => 1024,
            'path' => '../../img.jpg',
        ];
        $image = CardImage::fromArray($arr);
        $this->assertInstanceOf(CardImage::class, $image);
        $this->assertEquals(1, $image->movie_id);
        $this->assertEquals('https://example.com', $image->url);
        $this->assertEquals(680, $image->h);
        $this->assertEquals(1024, $image->w);
    }

    public function testKeyArtImageFromArray()
    {
        $arr = [
            'movie_id' => 1,
            'url' => 'https://example.com',
            'h' => 680,
            'w' => 1024,
            'path' => '../../img.jpg',
        ];
        $image = KeyArtImage::fromArray($arr);
        $this->assertInstanceOf(KeyArtImage::class, $image);
        $this->assertEquals(1, $image->movie_id);
        $this->assertEquals('https://example.com', $image->url);
        $this->assertEquals(680, $image->h);
        $this->assertEquals(1024, $image->w);
    }
}
