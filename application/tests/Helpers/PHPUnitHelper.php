<?php

namespace Tests\Helpers;

class PHPUnitHelper
{
    /**
     * @param $object
     * @param $methodName
     * @param array $args
     * @return mixed
     * @throws \ReflectionException
     */
    public static function callMethod($object, string $methodName, array $args = [])
    {
        $class = new \ReflectionClass($object);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);

        if (count($args)) {
            return $method->invokeArgs($object, $args);
        }
        return $method->invoke($object);
    }
}
