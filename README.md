Download project:    
    - get project from GIT: `git clone https://Pricopz@bitbucket.org/Pricopz/mgt.git` 
    
Install project:  
    - run `composer install`   
    - create new .env file `cp example.env .env`    
    - change variables data as you need


Install Running Docker  
    - `docker-compose build`    
    - `docker-compose up -d`    
    - `docker exec -it lmn_php bash`  
    - `cd lmn`  
    - `php artisan migrate`    
    - `exit`

Run Command to import DATA from JSON  
    - `docker exec -it lmn_php bash`  
    - `cd lmn`  
    - `php artisan config:cache --env`  
    - `php artisan feed:save`  
    - `exit`  
    
Run application URL  
    - `http://localhost/feed`  

Run Tests  
    - `docker exec -it lmn_php bash`  
    - `cd lmn`   
    - `php artisan config:cache --env=testing`   
    - `php artisan migrate`    
    - `vendor/bin/phpunit tests/Unit`  
    - `exit`  
